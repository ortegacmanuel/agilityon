class RoundsController < ApplicationController
  def index
    @rounds = Rounds::Round.all
  end

  def new
    @round_id = SecureRandom.uuid
  end

  def create
    cmd = RoundManagement::AddRound.new(
      round_id: params[:round_id],
      name: params[:name]
    )
    command_bus.call(cmd)

    redirect_to rounds_path, notice: "Round Added"
  end

  def manage
    @round = Rounds::Round.find_by_uid!(params[:id])
    @enrolled_dogs = Rounds::Dog.where(round_uid: params[:id])
    @dogs = Dogs::Dog.all
  end

  def enroll_dog
    cmd = RoundManagement::EnrollDogToRound.new(
      round_id: params[:id],
      dog_id: params[:dog_id]
    )
    command_bus.call(cmd)

    redirect_to manage_round_path(id:  params[:id]), notice: "Dog Enrolled"
  end

  def close
    cmd = RoundManagement::CloseRound.new(
      round_id: params[:id]
    )
    command_bus.call(cmd)

    redirect_to manage_round_path(id:  params[:id]), notice: "Round Closed"
  end
end
