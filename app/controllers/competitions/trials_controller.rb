module Competitions
  class TrialsController < ApplicationController
    def index
      @trials = Competitions::Trial.where(journey_id: params[:journey_id])
    end

    def new
      @journey = Competitions::Journey.find(params[:journey_id])
      @trial_id = SecureRandom.uuid
    end

    def create
      cmd = CompetitionManagement::AddTrialToCompetition.new(
        competition_id: params[:competition_id],
        journey_id: params[:journey_id],
        trial_id: params[:trial_id],
        level: params[:level]
      )
      command_bus.call(cmd)

      redirect_to journey_trials_path(params[:journey_id])
    end
  end
end