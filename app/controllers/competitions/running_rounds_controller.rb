module Competitions
  class RunningRoundsController < ApplicationController
    def index
      @rounds = Rounds::Round.where(competition_id: params[:competition_id])
    end

    def show
      @round = Rounds::Round.find_by_uid!(params[:id])
      @ranked_dogs = Rounds::Dog.where(round_uid: params[:id]).where(scribing_status: "finished").order(:rank)
    end
  end
end