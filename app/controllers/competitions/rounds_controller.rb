module Competitions
  class RoundsController < ApplicationController
    def index
      @rounds = Competitions::Round.where(trial_id: params[:trial_id])
    end

    def new
      @trial = Competitions::Trial.find(params[:trial_id])
      @round_id = SecureRandom.uuid
    end

    def create
      cmd = CompetitionManagement::AddRoundToCompetition.new(
        competition_id: params[:competition_id],
        trial_id: params[:trial_id],
        round_id: params[:round_id],
        name: params[:name]
      )
      command_bus.call(cmd)

      redirect_to trial_rounds_path(params[:trial_id])
    end
  end
end