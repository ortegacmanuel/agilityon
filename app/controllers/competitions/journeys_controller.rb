module Competitions
  class JourneysController < ApplicationController
    def index
      @journeys = Competitions::Journey.where(competition_id: params[:competition_id])
    end

    def new
      @journey_id = SecureRandom.uuid
    end

    def create
      cmd = CompetitionManagement::AddJourneyToCompetition.new(
        competition_id: params[:competition_id],
        journey_id: params[:journey_id],
        happening_at: Date.parse(params[:date])
      )
      command_bus.call(cmd)

      redirect_to competition_journeys_path(params[:competition_id])
    end
  end
end