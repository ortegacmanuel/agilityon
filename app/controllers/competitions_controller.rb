class CompetitionsController < ApplicationController
  def index
    @competitions = Competitions::Competition.all
  end

  def show
    @competition = Competitions::Competition.find(params[:id])
  end

  def new
    @competition_id = SecureRandom.uuid
  end

  def create
    cmd = CompetitionManagement::AddCompetition.new(
      competition_id: params[:competition_id],
      name: params[:name]
    )
    command_bus.call(cmd)

    redirect_to competitions_path, notice: "Competition Added"
  end

  def publish
    cmd = CompetitionManagement::PublishCompetition.new(
      competition_id: params[:id]
    )
    command_bus.call(cmd)

    redirect_to competition_path(params[:id]), notice: "Competition Published"
  end
end
