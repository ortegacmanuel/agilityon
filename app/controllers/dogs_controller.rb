class DogsController < ApplicationController
  def index
    @dogs = Dogs::Dog.all
  end

  def new
    @dog_id = SecureRandom.uuid
  end

  def create
    cmd = DogRegistry::AddDog.new(
      dog_id: params[:dog_id],
      name: params[:name]
    )
    command_bus.call(cmd)

    redirect_to dogs_path, notice: "Dog Added"
  end
end
