module P
  class RegistrationsController < ApplicationController
    def new
      @registration_request_id = SecureRandom.uuid
      @journeys = Competitions::Journey.where(competition_id: params[:competition_id])
    end

    def create
      cmd = Registrations::SubmitRegistrationRequest.new(
        competition_id: params[:competition_id],
        registration_request_id: params[:registration_request_id],
        journey_ids: params[:journey_ids].map {|journey_id| {journey_id: journey_id} },
        handler_id: params[:handler_id],
        dog_id: params[:dog_id]
      )
      command_bus.call(cmd)

      redirect_to p_competition_path(params[:competition_id])
    end


  end
end