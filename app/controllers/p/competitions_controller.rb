module P
  class CompetitionsController < ApplicationController
    def show
      @competition = Competitions::Competition.find(params[:id])
    end
  end
end