class ScribesheetsController < ApplicationController
  def create
    cmd = Scribing::StartScribing.new(
      scribesheet_id: params[:scribesheet_id],
      round_id: params[:round_id],
      dog_id: params[:dog_id],
    )
    command_bus.call(cmd)

    redirect_to scribesheet_path(params[:scribesheet_id])
  end

  def show
    @enrolled_dog = Rounds::Dog.find_by(scribesheet_id: params[:id])
    @round = Rounds::Round.find_by(uid: @enrolled_dog.round_uid)
  end

  def record_refusal
    cmd = Scribing::RecordRefusal.new(
      scribesheet_id: params[:scribesheet_id]
    )
    command_bus.call(cmd)

    redirect_to scribesheet_path(params[:scribesheet_id])
  end

  def record_fault
    cmd = Scribing::RecordFault.new(
      scribesheet_id: params[:scribesheet_id]
    )
    command_bus.call(cmd)

    redirect_to scribesheet_path(params[:scribesheet_id])
  end

  def record_elimination
    cmd = Scribing::RecordElimination.new(
      scribesheet_id: params[:scribesheet_id]
    )
    command_bus.call(cmd)

    redirect_to scribesheet_path(params[:scribesheet_id])
  end

  def record_dog_time
    cmd = Scribing::RecordDogTime.new(
      scribesheet_id: params[:scribesheet_id],
      seconds: params[:seconds].to_i,
      hundredths: params[:hundredths].to_i
    )
    command_bus.call(cmd)

    redirect_to scribesheet_path(params[:scribesheet_id])
  end

  def finish
    cmd = Scribing::FinishScribing.new(
      scribesheet_id: params[:scribesheet_id]
    )
    command_bus.call(cmd)

    enrolled_dog = Rounds::Dog.find_by(scribesheet_id: params[:scribesheet_id])

    redirect_to manage_round_path(id: enrolled_dog.round_uid)
  rescue Scribing::Scribesheet::DogTimeNotRecorded
    redirect_to scribesheet_path(params[:scribesheet_id]), alert: "You must record dog's time before finish"
  end
end
