module Competitions
  class Journey < ApplicationRecord
    self.table_name = "journeys"

    belongs_to :competition, class_name: "Competitions::Competition"
  end
end