module Competitions
  class OnTrialAddedToCompetition
    def call(event)
      Trial.create!(
        id: event.data[:trial_id],
        journey_id: event.data[:journey_id],
        level: event.data[:level]
      )
    end
  end
end