module Competitions
  class OnCompetitionPublished
    def call(event)
      competition = Competition.find(event.data[:competition_id])
      competition.update!(
        status: "Published",
        published_at: event.data[:published_at]
      )
    end
  end
end