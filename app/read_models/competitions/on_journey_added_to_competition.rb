module Competitions
  class OnJourneyAddedToCompetition
    def call(event)
      Journey.create!(
        id: event.data[:journey_id],
        competition_id: event.data[:competition_id],
        happening_at: event.data[:happening_at]
      )
    end
  end
end