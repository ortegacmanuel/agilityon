module Competitions
  class OnRoundAddedToCompetition
    def call(event)
      Round.create!(
        id: event.data[:round_id],
        trial_id: event.data[:trial_id],
        name: event.data[:name]
      )
    end
  end
end