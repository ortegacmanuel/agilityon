module Competitions
  class Round < ApplicationRecord
    self.table_name = "competition_rounds"

    belongs_to :trial, class_name: "Competitions::Trial"
  end
end