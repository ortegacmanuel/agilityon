module Competitions
  class OnCompetitionAdded
    def call(event)
      Competition.create!(
        id: event.data[:competition_id],
        name: event.data[:name],
        status: "Draft"
      )
    end
  end
end