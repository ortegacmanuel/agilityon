module Competitions
  class Trial < ApplicationRecord
    self.table_name = "trials"

    belongs_to :journey, class_name: "Competitions::Journey"
  end
end