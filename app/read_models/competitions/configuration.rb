module Competitions
  class Configuration
    def call(store)
      store.subscribe(Competitions::OnCompetitionAdded, to: [CompetitionManagement::CompetitionAdded])
      store.subscribe(Competitions::OnJourneyAddedToCompetition, to: [CompetitionManagement::JourneyAddedToCompetition])
      store.subscribe(Competitions::OnTrialAddedToCompetition, to: [CompetitionManagement::TrialAddedToCompetition])
      store.subscribe(Competitions::OnRoundAddedToCompetition, to: [CompetitionManagement::RoundAddedToCompetition])
      store.subscribe(Competitions::OnCompetitionPublished, to: [CompetitionManagement::CompetitionPublished])
    end
  end
end