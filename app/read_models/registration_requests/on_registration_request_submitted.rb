module RegistrationRequests
  class OnRegistrationRequestSubmitted
    def call(event)
      RegistrationRequests::RegistrationRequest.create!(
        id: event.data[:registration_request_id],
        competition_id: event.data[:competition_id],
        handler_id: event.data[:handler_id],
        dog_id: event.data[:dog_id],
        journey_ids: event.data[:journey_ids].map{|journey_id| journey_id[:journey_id] }
      )
    end
  end
end