module RegistrationRequests
  class RegistrationRequest < ApplicationRecord
    self.table_name = "competition_registration_requests"
  end
end