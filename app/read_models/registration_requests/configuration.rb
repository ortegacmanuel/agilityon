module RegistrationRequests
  class Configuration
    def call(store)
      store.subscribe(RegistrationRequests::OnRegistrationRequestSubmitted, to: [ Registrations::RegistrationRequestSubmitted])
    end
  end
end