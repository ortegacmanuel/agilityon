module Dogs
  class Configuration
    def call(store)
      store.subscribe(Dogs::OnDogAdded, to: [DogRegistry::DogAdded])
    end
  end
end