module Dogs
  class OnDogAdded
    def call(event)
      Dog.create!(
        uid: event.data[:dog_id],
        name: event.data[:name]
      )
    end
  end
end