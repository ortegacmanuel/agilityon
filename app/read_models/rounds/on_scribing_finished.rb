module Rounds
  class OnScribingFinished
    def call(event)
      enrolled_dog = Rounds::Dog.find_by(
        scribesheet_id: event.data[:scribesheet_id]
      )
      enrolled_dog.update!(scribing_status: "finished")
    end
  end
end