module Rounds
  class OnEliminationRecorded
    def call(event)
      enrolled_dog = Rounds::Dog.find_by(
        scribesheet_id: event.data[:scribesheet_id]
      )
      enrolled_dog.update!(eliminated: true)
    end
  end
end