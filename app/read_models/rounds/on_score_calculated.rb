module Rounds
  class OnScoreCalculated
    def call(event)
      enrolled_dog = Rounds::Dog.find_by(
        scribesheet_id: event.data[:scribesheet_id]
      )
      enrolled_dog.update!(score: event.data[:penalty_points])
    end
  end
end