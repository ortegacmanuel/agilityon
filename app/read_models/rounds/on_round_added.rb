module Rounds
  class OnRoundAdded
    def call(event)
      Round.create!(
        uid: event.data[:round_id],
        competition_id: event.data[:competition_id],
        name: event.data[:name],
        status: "open"
      )
    end
  end
end