module Rounds
  class OnRefusalRecorded
    def call(event)
      enrolled_dog = Rounds::Dog.find_by(
        scribesheet_id: event.data[:scribesheet_id]
      )
      enrolled_dog.increment!(:refusals_count)
    end
  end
end