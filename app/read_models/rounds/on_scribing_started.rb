module Rounds
  class OnScribingStarted
    def call(event)
      enrolled_dog = Rounds::Dog.where(
        uid: event.data[:dog_id],
        round_uid: event.data[:round_id]
      ).first
      enrolled_dog.update!(
        scribing_status: "in_progress",
        scribesheet_id: event.data[:scribesheet_id]
      )
    end
  end
end