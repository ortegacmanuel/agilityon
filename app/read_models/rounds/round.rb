module Rounds
  class Round < ApplicationRecord
    self.table_name = "rounds"
    self.primary_key = :uid

    has_many :enrolled_dogs,
      class_name: "Rounds::Dog",
      foreign_key: :round_uid
  end
end