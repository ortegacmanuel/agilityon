module Rounds
  class OnDogEnrolledToRound
    def call(event)
      round = Round.find_by_uid(event.data[:round_id])
      dog = Dogs::Dog.find_by_uid(event.data[:dog_id])
      round.increment!(:enrolled_dogs_count)
      round.enrolled_dogs.create!(
        uid: event.data[:dog_id],
        scribing_status: "not_started",
        name: dog.name
      )
    end
  end
end