module Rounds
  class Configuration
    def call(store)
      store.subscribe(Rounds::OnRoundAdded, to: [RoundManagement::RoundAdded])
      store.subscribe(Rounds::OnDogEnrolledToRound, to: [RoundManagement::DogEnrolledToRound])
      store.subscribe(Rounds::OnRoundClosed, to: [RoundManagement::RoundClosed])
      store.subscribe(Rounds::OnScribingStarted, to: [Scribing::ScribingStarted])
      store.subscribe(Rounds::OnRefusalRecorded, to: [Scribing::RefusalRecorded])
      store.subscribe(Rounds::OnFaultRecorded, to: [Scribing::FaultRecorded])
      store.subscribe(Rounds::OnDogTimeRecorded, to: [Scribing::DogTimeRecorded])
      store.subscribe(Rounds::OnEliminationRecorded, to: [Scribing::EliminationRecorded])
      store.subscribe(Rounds::OnScribingFinished, to: [Scribing::ScribingFinished])
      store.subscribe(Rounds::OnScoreCalculated, to: [Scoring::ScoreCalculated])
      store.subscribe(Rounds::OnDogRankedWithinRoundLeaderboard, to: [Ranking::DogRankedWithinRoundLeaderboard])
    end
  end
end