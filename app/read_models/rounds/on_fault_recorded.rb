module Rounds
  class OnFaultRecorded
    def call(event)
      enrolled_dog = Rounds::Dog.find_by(
        scribesheet_id: event.data[:scribesheet_id]
      )
      enrolled_dog.increment!(:faults_count)
    end
  end
end