module Rounds
  class OnDogRankedWithinRoundLeaderboard
    def call(event)
      event.data[:rankings].each do |ranking|
        enrolled_dog = Rounds::Dog.find_by(
          scribesheet_id: ranking[:scribesheet_id]
        )
        enrolled_dog.update!(rank: ranking[:rank])
      end
    end
  end
end