module Rounds
  class OnRoundClosed
    def call(event)
      round = Round.find_by_uid(event.data[:round_id])
      round.update!(status: "closed")
    end
  end
end