module Rounds
  class OnDogTimeRecorded
    def call(event)
      enrolled_dog = Rounds::Dog.find_by(
        scribesheet_id: event.data[:scribesheet_id]
      )
      dog_time = "#{event.data[:seconds]}.#{event.data[:hundredths]}"
      enrolled_dog.update!(dog_time: dog_time)
    end
  end
end