module Ranking
  class Configuration
    def call(bus)
      bus.register(Ranking::RankDogWithinRoundLeaderboard, Ranking::OnDogRankedWithinRoundLeaderboard.new)
    end
  end
end
