module Ranking
  class DogRankedWithinRoundLeaderboard < Event
    attribute :round_id, Types::UUID
    attribute :rankings, Types::Array do
      attribute :scribesheet_id, Types::UUID
      attribute :rank, Types::Integer
      attribute :score, Types::Integer
      attribute :time, Types::Integer
    end
  end
end