module Ranking
  class ScoringDog
    attr_reader :id, :score, :time

    def initialize(id, score, time)
      @id = id
      @score = score
      @time = time
    end
  end
end