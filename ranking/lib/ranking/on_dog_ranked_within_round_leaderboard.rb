module Ranking
  class OnDogRankedWithinRoundLeaderboard
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(RoundLeaderboard, command.aggregate_id) do |round_leaderboard|
        round_leaderboard.rank(command.scribesheet_id, command.score, command.time)
      end
    end
  end
end