module Ranking
  class RankedDog
    attr_reader :id, :score, :time, :rank

    def initialize(id, score, time, rank)
      @id = id
      @score = score
      @time = time
      @rank = rank
    end
  end
end