module Ranking
  class RankDogWithinRoundLeaderboard < Command
    attribute :round_id, Types::UUID
    attribute :scribesheet_id, Types::UUID
    attribute :score, Types::Integer
    attribute :time, Types::Integer


    alias :aggregate_id :round_id
  end
end