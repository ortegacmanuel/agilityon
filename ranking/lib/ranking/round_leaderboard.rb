module Ranking
  class RoundLeaderboard
    include AggregateRoot

    def initialize(id)
      @id = id
      @dogs = []
    end

    def rank(scribesheet_id, score, time)
      all_dogs = @dogs.push(ScoringDog.new(scribesheet_id, score, time))
      rankings = Ranker.rank(
        all_dogs,
        :by => lambda {|d| d.score },
        :asc => false,
        :strategy => Ranking::AgilityRankingStrategy
      )

      apply DogRankedWithinRoundLeaderboard.new(data: {
        round_id: @id,
        rankings: rankings.map {|r| r.rankables.map {|rable| { scribesheet_id: rable.id, score: rable.score, time: rable.time, rank: r.rank } } }.flatten
      })
    end

    on DogRankedWithinRoundLeaderboard do |event|
      @dogs = []
      event.data[:rankings].each do |ranking|
        @dogs << RankedDog.new(ranking[:scribesheet_id], ranking[:score], ranking[:time], ranking[:rank])
      end
    end
  end
end