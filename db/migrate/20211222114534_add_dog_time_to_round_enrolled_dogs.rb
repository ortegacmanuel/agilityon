class AddDogTimeToRoundEnrolledDogs < ActiveRecord::Migration[7.0]
  def change
    add_column :round_enrolled_dogs, :dog_time, :string
  end
end
