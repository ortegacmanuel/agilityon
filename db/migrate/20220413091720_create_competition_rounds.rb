class CreateCompetitionRounds < ActiveRecord::Migration[7.0]
  def change
    create_table :competition_rounds, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :trial_id, foreign_key: true
      t.string :name
    end
  end
end
