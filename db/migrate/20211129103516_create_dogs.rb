class CreateDogs < ActiveRecord::Migration[7.0]
  def change
    create_table :dogs, id: false do |t|
      t.uuid :uid
      t.string :name
    end

    add_index :dogs, :uid
  end
end
