class AddStatusToCompetitions < ActiveRecord::Migration[7.0]
  def change
    add_column :competitions, :status, :string
  end
end
