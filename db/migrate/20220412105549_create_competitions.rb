class CreateCompetitions < ActiveRecord::Migration[7.0]
  def change
    create_table :competitions, id: false do |t|
      t.uuid :id, primary_key: true
      t.string :name

      t.timestamps
    end
  end
end
