class CreateRounds < ActiveRecord::Migration[7.0]
  def change
    create_table :rounds, id: false do |t|
      t.uuid :uid
      t.string :name
    end

    add_index :rounds, :uid
  end
end
