class CreateJourneys < ActiveRecord::Migration[7.0]
  def change
    create_table :journeys, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :competition_id, foreign_key: true
      t.date :happening_at

      t.timestamps
    end
  end
end
