class CreateRoundEnrolledDogs < ActiveRecord::Migration[7.0]
  def change
    create_table :round_enrolled_dogs, id: false do |t|
      t.uuid :uid, foreign_key: true
      t.uuid :round_uid, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
