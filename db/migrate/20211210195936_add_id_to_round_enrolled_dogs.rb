class AddIdToRoundEnrolledDogs < ActiveRecord::Migration[7.0]
  def change
    add_column :round_enrolled_dogs, :id, :primary_key
  end
end
