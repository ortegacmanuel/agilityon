class CreateTrials < ActiveRecord::Migration[7.0]
  def change
    create_table :trials, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :journey_id, foreign_key: true
      t.string :level
    end
  end
end
