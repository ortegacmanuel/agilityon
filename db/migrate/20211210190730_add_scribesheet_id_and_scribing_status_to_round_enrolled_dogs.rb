class AddScribesheetIdAndScribingStatusToRoundEnrolledDogs < ActiveRecord::Migration[7.0]
  def change
    add_column :round_enrolled_dogs, :scribesheet_id, :uuid
    add_column :round_enrolled_dogs, :scribing_status, :string
  end
end
