class AddCompetitionIdToRounds < ActiveRecord::Migration[7.0]
  def change
    add_column :rounds, :competition_id, :uuid, foreign_key: true
  end
end
