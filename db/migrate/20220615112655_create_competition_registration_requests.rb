class CreateCompetitionRegistrationRequests < ActiveRecord::Migration[7.0]
  def change
    create_table :competition_registration_requests, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :competition_id, foreign_key: true
      t.uuid :dog_id, foreign_key: true
      t.uuid :handler_id, foreign_key: true
      t.uuid :journey_ids, array: true, default: []


      t.timestamps
    end
  end
end
