class AddEliminatedToRoundEnrolledDogs < ActiveRecord::Migration[7.0]
  def change
    add_column :round_enrolled_dogs, :eliminated, :boolean, null: false, default: false
  end
end
