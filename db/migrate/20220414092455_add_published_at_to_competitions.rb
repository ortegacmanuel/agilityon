class AddPublishedAtToCompetitions < ActiveRecord::Migration[7.0]
  def change
    add_column :competitions, :published_at, :date
  end
end
