class AddScoreAndRankToRoundEnrolledDogs < ActiveRecord::Migration[7.0]
  def change
    add_column :round_enrolled_dogs, :score, :integer
    add_column :round_enrolled_dogs, :rank, :integer
  end
end
