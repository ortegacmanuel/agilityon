class AddFaultsCountToRoundEnrolledDogs < ActiveRecord::Migration[7.0]
  def change
    add_column :round_enrolled_dogs, :faults_count, :integer, default: 0
  end
end
