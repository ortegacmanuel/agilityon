# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_06_15_112655) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "competition_registration_requests", id: :uuid, default: nil, force: :cascade do |t|
    t.uuid "competition_id"
    t.uuid "dog_id"
    t.uuid "handler_id"
    t.uuid "journey_ids", default: [], array: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "competition_rounds", id: :uuid, default: nil, force: :cascade do |t|
    t.uuid "trial_id"
    t.string "name"
  end

  create_table "competitions", id: :uuid, default: nil, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "status"
    t.date "published_at"
  end

  create_table "dogs", id: false, force: :cascade do |t|
    t.uuid "uid"
    t.string "name"
    t.index ["uid"], name: "index_dogs_on_uid"
  end

  create_table "event_store_events", force: :cascade do |t|
    t.uuid "event_id", null: false
    t.string "event_type", null: false
    t.binary "metadata"
    t.binary "data", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "valid_at", precision: 6
    t.index ["created_at"], name: "index_event_store_events_on_created_at"
    t.index ["event_id"], name: "index_event_store_events_on_event_id", unique: true
    t.index ["event_type"], name: "index_event_store_events_on_event_type"
    t.index ["valid_at"], name: "index_event_store_events_on_valid_at"
  end

  create_table "event_store_events_in_streams", force: :cascade do |t|
    t.string "stream", null: false
    t.integer "position"
    t.uuid "event_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.index ["created_at"], name: "index_event_store_events_in_streams_on_created_at"
    t.index ["stream", "event_id"], name: "index_event_store_events_in_streams_on_stream_and_event_id", unique: true
    t.index ["stream", "position"], name: "index_event_store_events_in_streams_on_stream_and_position", unique: true
  end

  create_table "journeys", id: :uuid, default: nil, force: :cascade do |t|
    t.uuid "competition_id"
    t.date "happening_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "round_enrolled_dogs", force: :cascade do |t|
    t.uuid "uid"
    t.uuid "round_uid"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "scribesheet_id"
    t.string "scribing_status"
    t.integer "refusals_count", default: 0
    t.integer "faults_count", default: 0
    t.string "dog_time"
    t.boolean "eliminated", default: false, null: false
    t.integer "score"
    t.integer "rank"
  end

  create_table "rounds", id: false, force: :cascade do |t|
    t.uuid "uid"
    t.string "name"
    t.integer "enrolled_dogs_count", default: 0
    t.string "status"
    t.uuid "competition_id"
    t.index ["uid"], name: "index_rounds_on_uid"
  end

  create_table "trials", id: :uuid, default: nil, force: :cascade do |t|
    t.uuid "journey_id"
    t.string "level"
  end

end
