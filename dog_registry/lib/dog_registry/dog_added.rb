module DogRegistry
  class DogAdded < Event
    attribute :dog_id, Types::UUID
    attribute :name, Types::String
  end
end