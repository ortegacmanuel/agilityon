module DogRegistry
  class Dog
    include AggregateRoot

    def initialize(id)
      @id = id
    end

    def add(name)
      apply DogAdded.new(data: {
        dog_id: @id,
        name: name
      })
    end

    on DogAdded do |event|
      @name = event.data[:name]
    end
  end
end