module DogRegistry
  class OnDogAdded
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Dog, command.aggregate_id) do |dog|
        dog.add(command.name)
      end
    end
  end
end