module DogRegistry
  class AddDog < Command
    attribute :dog_id, Types::UUID
    attribute :name, Types::String

    alias :aggregate_id :dog_id
  end
end