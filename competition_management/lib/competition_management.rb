module CompetitionManagement
  class Configuration
    def call(bus)
      bus.register(CompetitionManagement::AddCompetition, CompetitionManagement::OnCompetitionAdded.new)
      bus.register(CompetitionManagement::AddJudgeToCompetition, CompetitionManagement::OnJudgeAddedToCompetition.new)
      bus.register(CompetitionManagement::AddJourneyToCompetition, CompetitionManagement::OnJourneyAddedToCompetition.new)
      bus.register(CompetitionManagement::AddTrialToCompetition, CompetitionManagement::OnTrialAddedToCompetition.new)
      bus.register(CompetitionManagement::AddRoundToCompetition, CompetitionManagement::OnRoundAddedToCompetition.new)
      bus.register(CompetitionManagement::PublishCompetition, CompetitionManagement::OnCompetitionPublished.new)
    end
  end
end
