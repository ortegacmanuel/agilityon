module CompetitionManagement
  class OnTrialAddedToCompetition
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Competition, command.aggregate_id) do |competition|
        competition.add_trial(command.journey_id, command.trial_id, command.level)
      end
    end
  end
end