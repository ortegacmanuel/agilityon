module CompetitionManagement
  class Round

    def initialize(id, name)
      @id = id
      @name = name
    end

    def to_hash
      {
        round_id: @id,
        name: @name
      }
    end
  end
end