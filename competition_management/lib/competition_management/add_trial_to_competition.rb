module CompetitionManagement
  class AddTrialToCompetition < Command
    attribute :competition_id, Types::UUID
    attribute :journey_id, Types::UUID
    attribute :trial_id, Types::UUID
    attribute :level, Types::Strict::String

    alias :aggregate_id :competition_id
  end
end