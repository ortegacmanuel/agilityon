module CompetitionManagement
  class Journey
    attr_reader :id, :trials

    def initialize(id, happening_at)
      @id = id
      @happening_at = happening_at
      @trials = []
    end

    def add_trial(trial_id, level)
      @trials << Trial.new(trial_id, level)
    end

    def find_trial(trial_id)
      trials.find {|t| t.id === trial_id }
    end

    def to_hash
      {
        journey_id: @id,
        happening_at: @happening_at,
        trials: @trials.map(&:to_hash)
      }
    end
  end
end