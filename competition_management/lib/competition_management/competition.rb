module CompetitionManagement
  class Competition
    include AggregateRoot

    def initialize(id)
      @id = id
      @state = :in_progress
      @judges = []
      @journeys = []
    end

    def add(name)
      apply CompetitionAdded.new(data: {
        competition_id: @id,
        name: name
      })
    end

    def add_judge(judge_id)
      apply JudgeAddedToCompetition.new(data: {
        competition_id: @id,
        judge_id: judge_id
      })
    end

    def add_journey(journey_id, happening_at)
      apply JourneyAddedToCompetition.new(data: {
        competition_id: @id,
        journey_id: journey_id,
        happening_at: happening_at
      })
    end

    def add_trial(journey_id, trial_id, level)
      apply TrialAddedToCompetition.new(data: {
        competition_id: @id,
        journey_id: journey_id,
        trial_id: trial_id,
        level: level
      })
    end

    def add_round(trial_id, round_id, name)
      apply RoundAddedToCompetition.new(data: {
        competition_id: @id,
        trial_id: trial_id,
        round_id: round_id,
        name: name
      })
    end

    def publish
      apply CompetitionPublished.new(data: {
        competition_id: @id,
        published_at: Date.today,
        journeys: @journeys.map(&:to_hash)
      })
    end

    def trials
      @journeys.map(&:trials).flatten
    end

    on CompetitionAdded do |event|
      @name = event.data[:name]
    end

    on JudgeAddedToCompetition do |event|
      @judges << event.data[:judge_id]
    end

    on JourneyAddedToCompetition do |event|
      @journeys << Journey.new(event.data[:journey_id], event.data[:happening_at])
    end

    on TrialAddedToCompetition do |event|
      journey = @journeys.find {|j| j.id == event.data[:journey_id] }

      journey.add_trial(
        event.data[:trial_id],
        event.data[:level]
      )
    end

    on RoundAddedToCompetition do |event|
      trial = trials.find {|t| t.id == event.data[:trial_id] }

      trial.add_round(
        event.data[:round_id],
        event.data[:name]
      )
    end

    on CompetitionPublished do |event|
      @published_at = event.data[:published_at]
      @state = :published
    end
  end
end