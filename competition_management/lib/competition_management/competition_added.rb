module CompetitionManagement
  class CompetitionAdded < Event
    attribute :competition_id, Types::UUID
    attribute :name, Types::String
  end
end