module CompetitionManagement
  class JourneyAddedToCompetition < Event
    attribute :competition_id, Types::UUID
    attribute :journey_id, Types::UUID
    attribute :happening_at, Types::Strict::Date
  end
end