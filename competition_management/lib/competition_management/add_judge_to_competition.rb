module CompetitionManagement
  class AddJudgeToCompetition < Command
    attribute :competition_id, Types::UUID
    attribute :judge_id, Types::UUID

    alias :aggregate_id :competition_id
  end
end