module CompetitionManagement
  class AddCompetition < Command
    attribute :competition_id, Types::UUID
    attribute :name, Types::String

    alias :aggregate_id :competition_id
  end
end