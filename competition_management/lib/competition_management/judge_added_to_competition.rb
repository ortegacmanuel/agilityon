module CompetitionManagement
  class JudgeAddedToCompetition < Event
    attribute :competition_id, Types::UUID
    attribute :judge_id, Types::UUID
  end
end