module CompetitionManagement
  class RoundAddedToCompetition < Event
    attribute :competition_id, Types::UUID
    attribute :trial_id, Types::UUID
    attribute :round_id, Types::UUID
    attribute :name, Types::Strict::String
  end
end