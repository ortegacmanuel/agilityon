module CompetitionManagement
  class OnJourneyAddedToCompetition
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Competition, command.aggregate_id) do |competition|
        competition.add_journey(command.journey_id, command.happening_at)
      end
    end
  end
end