module CompetitionManagement
  class PublishCompetition < Command
    attribute :competition_id, Types::UUID

    alias :aggregate_id :competition_id
  end
end