module CompetitionManagement
  class CompetitionPublished < Event
    attribute :competition_id, Types::UUID
    attribute :published_at, Types::Strict::Date
    attribute :journeys, Types::Array do
      attribute :journey_id, Types::UUID
      attribute :happening_at, Types::Strict::Date
      attribute :trials, Types::Array do
        attribute :trial_id, Types::UUID
        attribute :level, Types::String
        attribute :rounds, Types::Array do
          attribute :round_id, Types::UUID
          attribute :name, Types::String
        end
      end
    end
  end
end