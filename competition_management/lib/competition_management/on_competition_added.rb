module CompetitionManagement
  class OnCompetitionAdded
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Competition, command.aggregate_id) do |competition|
        competition.add(command.name)
      end
    end
  end
end