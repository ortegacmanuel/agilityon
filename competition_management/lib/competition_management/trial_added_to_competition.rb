module CompetitionManagement
  class TrialAddedToCompetition < Event
    attribute :competition_id, Types::UUID
    attribute :journey_id, Types::UUID
    attribute :trial_id, Types::UUID
    attribute :level, Types::Strict::String
  end
end