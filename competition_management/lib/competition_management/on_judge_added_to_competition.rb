module CompetitionManagement
  class OnJudgeAddedToCompetition
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Competition, command.aggregate_id) do |competition|
        competition.add_judge(command.judge_id)
      end
    end
  end
end