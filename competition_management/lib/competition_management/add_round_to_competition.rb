module CompetitionManagement
  class AddRoundToCompetition < Command
    attribute :competition_id, Types::UUID
    attribute :trial_id, Types::UUID
    attribute :round_id, Types::UUID
    attribute :name, Types::Strict::String

    alias :aggregate_id :competition_id
  end
end