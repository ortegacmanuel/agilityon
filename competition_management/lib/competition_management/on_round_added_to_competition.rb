module CompetitionManagement
  class OnRoundAddedToCompetition
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Competition, command.aggregate_id) do |competition|
        competition.add_round(command.trial_id, command.round_id, command.name)
      end
    end
  end
end