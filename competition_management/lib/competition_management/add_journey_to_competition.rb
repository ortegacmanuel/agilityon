module CompetitionManagement
  class AddJourneyToCompetition < Command
    attribute :competition_id, Types::UUID
    attribute :journey_id, Types::UUID
    attribute :happening_at, Types::Strict::Date

    alias :aggregate_id :competition_id
  end
end