module CompetitionManagement
  class Trial
    attr_reader :id, :rounds

    def initialize(id, level)
      @id = id
      @level = level
      @rounds = []
    end

    def add_round(round_id, name)
      @rounds << Round.new(round_id, name)
    end

    def find_round(round_id)
      rounds.find {|r| r.id === round_id }
    end

    def to_hash
      {
        trial_id: @id,
        level: @level,
        rounds: @rounds.map(&:to_hash)
      }
    end
  end
end