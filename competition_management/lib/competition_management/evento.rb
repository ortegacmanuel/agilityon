module EventManagement
  class Evento
    include AggregateRoot

    def initialize(id)
      @id = id
      @state = :in_progress
      @judges = []
      @journeys = []
    end

    def add(name)
      @name = name
    end

    def add_judge(judge_id)
      @judges << judge_id
    end

    def add_journey(journey_id, date)
      @journeys << Journey.new(journey_id, date)
    end

    def add_trial(journey_id, trial_id, level)
      journey = @journeys.find {|j| j.id == journey_id }

      journey.add_trial(trial_id, level)
    end

    def add_round(trial_id, round_id, name, jump_height)
      trial = trials.find {|t| t.id == trial_id }

      trial.add_round(round_id, name, jump_height)
    end

    def publish
      @published_at = DateTime.now
      @state = :published
    end

    def trials
      @journeys.map(&:trials).flatten
    end
  end
end