module CompetitionManagement
  class OnCompetitionPublished
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Competition, command.aggregate_id) do |competition|
        competition.publish
      end
    end
  end
end