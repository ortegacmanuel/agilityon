module RoundManagement
  class CloseRound < Command
    attribute :round_id, Types::UUID

    alias :aggregate_id :round_id
  end
end