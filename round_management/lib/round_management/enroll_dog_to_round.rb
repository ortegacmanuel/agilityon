module RoundManagement
  class EnrollDogToRound < Command
    attribute :round_id, Types::UUID
    attribute :dog_id, Types::UUID

    alias :aggregate_id :round_id
  end
end