module RoundManagement
  class RoundClosed < Event
    attribute :round_id, Types::UUID
  end
end