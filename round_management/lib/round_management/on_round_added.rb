module RoundManagement
  class OnRoundAdded
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Round, command.aggregate_id) do |round|
        round.add(
          command.name,
          command.competition_id,
          command.journey_id,
          command.trial_id,
          command.category
        )
      end
    end
  end
end