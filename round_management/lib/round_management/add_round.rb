module RoundManagement
  class AddRound < Command
    attribute :round_id, Types::UUID
    attribute :name, Types::String
    attribute :competition_id, Types::UUID
    attribute :journey_id, Types::UUID
    attribute :trial_id, Types::UUID
    attribute :category, Types::String

    alias :aggregate_id :round_id
  end
end