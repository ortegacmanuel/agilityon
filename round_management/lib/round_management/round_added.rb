module RoundManagement
  class RoundAdded < Event
    attribute :round_id, Types::UUID
    attribute :name, Types::String
    attribute :competition_id, Types::UUID
    attribute :journey_id, Types::UUID
    attribute :trial_id, Types::UUID
    attribute :category, Types::String
  end
end