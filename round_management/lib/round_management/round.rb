module RoundManagement
  class Round
    include AggregateRoot

    def initialize(id)
      @id = id
      @state = :open
      @running_dogs = []
    end

    def add(name, competition_id, journey_id, trial_id, category)
      apply RoundAdded.new(data: {
        round_id: @id,
        name: name,
        competition_id: competition_id,
        journey_id: journey_id,
        trial_id: trial_id,
        category: category
      })
    end

    def enroll_dog(dog_id)
      apply DogEnrolledToRound.new(data: {
        round_id: @id,
        dog_id: dog_id
      })
    end

    def close
      apply RoundClosed.new(data: {
        round_id: @id
      })
    end

    on RoundAdded do |event|
      @name = event.data[:name]
      @competition_id = event.data[:competition_id]
      @journey_id = event.data[:journey_id]
      @trial_id = event.data[:trial_id]
      @category = event.data[:category]
    end

    on DogEnrolledToRound do |event|
      @running_dogs << event.data[:dog_id]
    end

    on RoundClosed do |event|
      @state = :closed
    end
  end
end