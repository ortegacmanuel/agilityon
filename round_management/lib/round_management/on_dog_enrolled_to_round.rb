module RoundManagement
  class OnDogEnrolledToRound
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Round, command.aggregate_id) do |round|
        round.enroll_dog(command.dog_id)
      end
    end
  end
end