module RoundManagement
  class OnRoundClosed
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Round, command.aggregate_id) do |round|
        round.close
      end
    end
  end
end