module RoundManagement
  class DogEnrolledToRound < Event
    attribute :round_id, Types::UUID
    attribute :dog_id, Types::UUID
  end
end