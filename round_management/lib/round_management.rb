module RoundManagement
  class Configuration
    def call(bus)
      bus.register(RoundManagement::AddRound, RoundManagement::OnRoundAdded.new)
      bus.register(RoundManagement::EnrollDogToRound, RoundManagement::OnDogEnrolledToRound.new)
      bus.register(RoundManagement::CloseRound, RoundManagement::OnRoundClosed.new)
    end
  end
end
