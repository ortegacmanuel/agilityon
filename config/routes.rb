Rails.application.routes.draw do
  mount RailsEventStore::Browser => '/res' if Rails.env.development?

  resources :dogs, only: [:index, :new, :create]

  resources :competitions, only: [:index, :new, :create, :show] do
    resources :journeys, only: [:index, :new, :create], module: :competitions
    resources :running_rounds, only: [:index, :show], module: :competitions
    member do
      post "publish"
    end
  end
  resources :journeys, only: [], module: :competitions do
    resources :trials, only: [:index, :new, :create]
  end
  resources :trials, only: [], module: :competitions do
    resources :rounds, only: [:index, :new, :create]
  end
  resources :rounds, only: [:index, :new, :create] do
    member do
      get "manage"
      post "enroll_dog"
      get "close"
    end
  end

  resources :scribesheets, only: [:show, :create] do
    member do
      post "record_refusal"
      post "record_fault"
      post "record_dog_time"
      post "record_elimination"
      post "finish"
    end
  end

  namespace :p do
    resources :competitions, only: [:show] do
      resources :registrations, only: [:new, :create]
    end
  end

  root "home#index"
end
