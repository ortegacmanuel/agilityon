require 'rails_event_store'
require 'aggregate_root'
require 'arkency/command_bus'

Rails.configuration.to_prepare do
  Rails.configuration.event_store = RailsEventStore::Client.new
  Rails.configuration.command_bus = Arkency::CommandBus.new

  AggregateRoot.configure do |config|
    config.default_event_store = Rails.configuration.event_store
  end

  # Subscribe event handlers below
  Rails.configuration.event_store.tap do |store|
    # store.subscribe(InvoiceReadModel.new, to: [InvoicePrinted])
    # store.subscribe(lambda { |event| SendOrderConfirmation.new.call(event) }, to: [OrderSubmitted])
    # store.subscribe_to_all_events(lambda { |event| Rails.logger.info(event.event_type) })

    store.subscribe_to_all_events(RailsEventStore::LinkByEventType.new)
    store.subscribe_to_all_events(RailsEventStore::LinkByCorrelationId.new)
    store.subscribe_to_all_events(RailsEventStore::LinkByCausationId.new)

  # Read Models
    Dogs::Configuration.new.call(store)
    Competitions::Configuration.new.call(store)
    Rounds::Configuration.new.call(store)
    RegistrationRequests::Configuration.new.call(store)

    store.subscribe(
      ->(event) do
        Rails.configuration.command_bus.call(
          Scoring::CalculateScore.new(
            scribesheet_id: event.data.fetch(:scribesheet_id),
            round_id: event.data.fetch(:round_id),
            dog_id: event.data.fetch(:dog_id),
            faults: event.data.fetch(:faults),
            refusals: event.data.fetch(:refusals),
            seconds: event.data.fetch(:seconds),
            hundredths: event.data.fetch(:hundredths),
            eliminated: event.data.fetch(:eliminated)

          )
        )
      end,
      to: [Scribing::ScribingFinished]
    )
    store.subscribe(
      ->(event) do
        Rails.configuration.command_bus.call(
          Ranking::RankDogWithinRoundLeaderboard.new(
            scribesheet_id: event.data.fetch(:scribesheet_id),
            round_id: event.data.fetch(:round_id),
            score: event.data.fetch(:penalty_points),
            time: (event.data.fetch(:seconds) * 100) + event.data.fetch(:hundredths),
          )
        )
      end,
      to: [Scoring::ScoreCalculated]
    )
  end

  # Register command handlers below
  Rails.configuration.command_bus.tap do |bus|
    # bus.register(PrintInvoice, Invoicing::OnPrint.new)
    # bus.register(SubmitOrder,  ->(cmd) { Ordering::OnSubmitOrder.new.call(cmd) })

    bus.register(DogRegistry::AddDog, DogRegistry::OnDogAdded.new)

    RoundManagement::Configuration.new.call(bus)
    CompetitionManagement::Configuration.new.call(bus)
    Ranking::Configuration.new.call(bus)
    Scribing::Configuration.new.call(bus)
    Scoring::Configuration.new.call(bus)
    Registrations::Configuration.new.call(bus)
  end

  Processes::Configuration.new.call(
    Rails.configuration.event_store,
    Rails.configuration.command_bus
  )
end
