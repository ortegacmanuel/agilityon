require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Agilityon
  class Application < Rails::Application
    config.paths.add 'registrations/lib', eager_load: true
    config.paths.add 'scoring/lib', eager_load: true
    config.paths.add 'scribing/lib', eager_load: true
    config.paths.add 'round_management/lib', eager_load: true
    config.paths.add 'competition_management/lib', eager_load: true
    config.paths.add 'ranking/lib', eager_load: true
    config.paths.add 'processes/lib', eager_load: true
    config.paths.add "lib", eager_load: true
    config.paths.add 'dog_registry/lib', eager_load: true
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
