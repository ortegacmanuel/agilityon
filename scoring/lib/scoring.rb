module Scoring
  class Configuration
    def call(bus)
      bus.register(Scoring::CalculateScore, Scoring::OnScoreCalculated.new)
    end
  end
end
