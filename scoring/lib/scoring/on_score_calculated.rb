module Scoring
  class OnScoreCalculated
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Scribesheet, command.aggregate_id) do |scribesheet|
        scribesheet.calculate_score(
          command.round_id,
          command.dog_id,
          command.faults,
          command.refusals,
          command.seconds,
          command.hundredths,
          command.eliminated
        )
      end
    end
  end
end