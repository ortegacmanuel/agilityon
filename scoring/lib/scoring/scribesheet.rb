module Scoring
  class Scribesheet
    include AggregateRoot

    DogTimeNotRecorded = Class.new(StandardError)

    def initialize(id)
      @id = id
    end

    def calculate_score(round_id, dog_id, faults, refusals, seconds, hundredths, eliminated)
      penalty_points = eliminated ? 100 : ((faults + refusals) * 5)

      case penalty_points
      when 0..5.99
        qualification = "excellent"
      when 6..15.99
        qualification = "very_good"
      when 16..25.99
        qualification = "good"
      else
        qualification = "no_qualification"
      end

      apply ScoreCalculated.new(data: {
        scribesheet_id: @id,
        dog_id: dog_id,
        round_id: round_id,
        seconds: seconds,
        hundredths: hundredths,
        faults: faults,
        refusals: refusals,
        eliminated: eliminated,
        penalty_points: penalty_points,
        qualification: qualification
      })
    end

    on ScoreCalculated do |event|
      @round_id = event.data[:round_id]
      @dog_id = event.data[:dog_id]
      @state = :started
    end
  end
end