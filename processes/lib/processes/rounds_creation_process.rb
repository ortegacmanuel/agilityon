module Processes
  class RoundsCreationProcess
    def initialize(store, command_bus)
      @store = store
      @command_bus = command_bus
    end

    def call(event)
      state = build_state(event)
      categories = [
        "XS",
        "S",
        "M",
        "L",
        "XL"
      ]
      categories.each do |category|
        state.rounds.each do |round|
          command = RoundManagement::AddRound.new(
            round_id: SecureRandom.uuid,
            name: "#{round[:trial_level]} / #{category} / #{round[:name]}",
            competition_id: state.competition_id,
            journey_id: round[:journey_id],
            trial_id: round[:trial_id],
            category: category
          )
          command_bus.call(command)
        end
      end
    end

    private

    attr_reader :store
    attr_reader :command_bus

    def build_state(event)
      stream_name = "RoundsCreationProcess$#{event.data.fetch(:competition_id)}"
      # read all events from stream
      past_events = store.read.stream(stream_name).to_a
      last_stored = past_events.size - 1
      # link_event_to_stream
      store.link(event.event_id, stream_name: stream_name, expected_version: :any)
      ProcessState.new.tap do |state|
        past_events.each { |ev| state.call(ev) }
        state.call(event)
      end
    rescue RubyEventStore::WrongExpectedEventVersion
      retry
    end

    class ProcessState
      def initialize
        @rounds = []
      end

      attr_reader :rounds
      attr_reader :competition_id

      def call(event)
        case event
        when CompetitionManagement::CompetitionPublished
          @competition_id = event.data[:competition_id]
          event.data[:journeys].each do |j|
            j[:trials].each do |t|
              t[:rounds].each do |r|
                round = {
                  journey_id: j[:journey_id],
                  trial_id: t[:trial_id],
                  trial_level: t[:level],
                  name: r[:name]
                }
                @rounds << round
              end
            end
          end
        end
      end
    end
  end
end