module Processes
  class Configuration
    def call(store, command_bus)
      store.subscribe(
        RoundsCreationProcess.new(store, command_bus),
        to: [CompetitionManagement::CompetitionPublished]
      )
    end
  end
end