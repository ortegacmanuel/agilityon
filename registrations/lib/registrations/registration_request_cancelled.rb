module Registrations
  class RegistrationRequestCancelled < Event
    attribute :registration_request_id, Types::UUID
    attribute :note, Types::String
  end
end