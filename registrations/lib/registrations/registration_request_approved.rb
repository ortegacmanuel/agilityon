module Registrations
  class RegistrationRequestApproved < Event
    attribute :registration_request_id, Types::UUID
  end
end