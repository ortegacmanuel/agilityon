module Registrations
  class RegistrationRequestChangesRequested < Event
    attribute :registration_request_id, Types::UUID
    attribute :note, Types::String
  end
end