module Registrations
  class OnRegistrationRequestSubmitted
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(RegistrationRequest, command.aggregate_id) do |registration_request|
        registration_request.submit(
          command.competition_id,
          command.handler_id,
          command.dog_id,
          command.journey_ids
        )
      end
    end
  end
end