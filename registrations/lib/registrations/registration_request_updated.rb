module Registrations
  class RegistrationRequestUpdated < Event
    attribute :registration_request_id, Types::UUID
    attribute :competition_id, Types::UUID
    attribute :handler_id, Types::UUID
    attribute :dog_id, Types::UUID
    attribute :journey_ids, Types::Array do
      attribute :journey_id, Types::UUID
    end
  end
end