module Registrations
  class RegistrationRequest
    include AggregateRoot

    CannotCancelAlreadyApproved = Class.new(StandardError)
    CannotCancelNotSubmitted = Class.new(StandardError)
    CannotCancelAlreadyCancelled = Class.new(StandardError)

    CannotAproveAlreadyCancelled = Class.new(StandardError)
    CannotAproveAlreadyApproved = Class.new(StandardError)
    CannotAproveNotSubmitted = Class.new(StandardError)

    CannotRequestChangesAlreadyCancelled = Class.new(StandardError)
    CannotRequestChangesAlreadyApproved = Class.new(StandardError)
    CannotRequestChangesNotSubmitted = Class.new(StandardError)

    CannotUpdateAlreadyCancelled = Class.new(StandardError)
    CannotUpdateAlreadyApproved = Class.new(StandardError)

    def initialize(id)
      @id = id
      @state = :draft
    end

    def submit(competition_id, handler_id, dog_id, journey_ids)
      apply RegistrationRequestSubmitted.new(data: {
        registration_request_id: @id,
        competition_id: competition_id,
        handler_id: handler_id,
        dog_id: dog_id,
        journey_ids: journey_ids
      })
    end

    def approve
      raise CannotAproveNotSubmitted if @state == :draft
      raise CannotAproveAlreadyCancelled if @state == :cancelled
      raise CannotAproveAlreadyApproved if @state == :approved

      apply RegistrationRequestApproved.new(data: {
        registration_request_id: @id
      })
    end

    def cancel(note = "")
      raise CannotCancelNotSubmitted if @state == :draft
      raise CannotCancelAlreadyApproved if @state == :approved
      raise CannotCancelAlreadyCancelled if @state == :cancelled

      apply RegistrationRequestCancelled.new(data: {
        registration_request_id: @id,
        note: note
      })
    end

    def request_for_changes(note = "")
      raise CannotRequestChangesNotSubmitted if @state == :draft
      raise CannotRequestChangesAlreadyCancelled if @state == :cancelled
      raise CannotRequestChangesAlreadyApproved if @state == :approved

      apply RegistrationRequestChangesRequested.new(data: {
        registration_request_id: @id,
        note: note
      })
    end

    def update(journey_ids)
      raise CannotUpdateAlreadyCancelled if @state == :cancelled
      raise CannotUpdateAlreadyApproved if @state == :approved

      apply RegistrationRequestUpdated.new(data: {
        registration_request_id: @id,
        competition_id: @competition_id,
        handler_id: @handler_id,
        dog_id: @dog_id,
        journey_ids: journey_ids
      })
    end



    on RegistrationRequestSubmitted do |event|
      @competition_id = event.data[:competition_id]
      @handler_id = event.data[:handler_id]
      @dog_id = event.data[:dog_id]
      @journey_ids = event.data[:journey_ids]
      @state = :submitted
    end

    on RegistrationRequestApproved do |event|
      @state = :approved
    end

    on RegistrationRequestCancelled do |event|
      @note = event.data[:note]
      @state = :cancelled
    end

    on RegistrationRequestChangesRequested do |event|
      @note = event.data[:note]
      @state = :changes_requested
    end

    on RegistrationRequestUpdated do |event|
      @journey_ids = event.data[:journey_ids]
      @state = :submitted
    end
  end
end