module Registrations
  class Configuration
    def call(bus)
      bus.register(Registrations::SubmitRegistrationRequest, Registrations::OnRegistrationRequestSubmitted.new)
    end
  end
end
