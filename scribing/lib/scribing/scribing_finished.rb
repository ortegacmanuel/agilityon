module Scribing
  class ScribingFinished < Event
    attribute :scribesheet_id, Types::UUID
    attribute :dog_id, Types::UUID
    attribute :round_id, Types::UUID
    attribute :faults, Types::Strict::Integer
    attribute :refusals, Types::Strict::Integer
    attribute :seconds, Types::Strict::Integer
    attribute :hundredths, Types::Strict::Integer
    attribute :eliminated, Types::Strict::Bool
  end
end