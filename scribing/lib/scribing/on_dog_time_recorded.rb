module Scribing
  class OnDogTimeRecorded
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Scribesheet, command.aggregate_id) do |scribesheet|
        scribesheet.record_dog_time(command.seconds, command.hundredths)
      end
    end
  end
end