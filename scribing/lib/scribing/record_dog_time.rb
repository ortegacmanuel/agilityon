module Scribing
  class RecordDogTime < Command
    attribute :scribesheet_id, Types::UUID
    attribute :seconds, Types::Strict::Integer
    attribute :hundredths, Types::Strict::Integer

    alias :aggregate_id :scribesheet_id
  end
end