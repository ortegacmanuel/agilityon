module Scribing
  class OnEliminationRecorded
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Scribesheet, command.aggregate_id) do |scribesheet|
        scribesheet.record_elimination
      end
    end
  end
end