module Scribing
  class DogTimeRecorded < Event
    attribute :scribesheet_id, Types::UUID
    attribute :seconds, Types::Strict::Integer
    attribute :hundredths, Types::Strict::Integer
  end
end