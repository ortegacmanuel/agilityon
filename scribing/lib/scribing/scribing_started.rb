module Scribing
  class ScribingStarted < Event
    attribute :scribesheet_id, Types::UUID
    attribute :round_id, Types::UUID
    attribute :dog_id, Types::UUID
  end
end