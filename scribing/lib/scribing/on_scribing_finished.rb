module Scribing
  class OnScribingFinished
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Scribesheet, command.aggregate_id) do |scribesheet|
        scribesheet.finish
      end
    end
  end
end