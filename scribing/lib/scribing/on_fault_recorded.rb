module Scribing
  class OnFaultRecorded
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Scribesheet, command.aggregate_id) do |scribesheet|
        scribesheet.record_fault
      end
    end
  end
end