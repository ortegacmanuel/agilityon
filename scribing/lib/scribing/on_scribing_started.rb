module Scribing
  class OnScribingStarted
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Scribesheet, command.aggregate_id) do |scribesheet|
        scribesheet.start(command.round_id, command.dog_id)
      end
    end
  end
end