module Scribing
  class StartScribing < Command
    attribute :scribesheet_id, Types::UUID
    attribute :round_id, Types::UUID
    attribute :dog_id, Types::UUID

    alias :aggregate_id :scribesheet_id
  end
end