module Scribing
  class RefusalRecorded < Event
    attribute :scribesheet_id, Types::UUID
  end
end