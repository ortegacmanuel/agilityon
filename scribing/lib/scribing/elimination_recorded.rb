module Scribing
  class EliminationRecorded < Event
    attribute :scribesheet_id, Types::UUID
  end
end