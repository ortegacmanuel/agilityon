module Scribing
  class Scribesheet
    include AggregateRoot

    DogTimeNotRecorded = Class.new(StandardError)

    def initialize(id)
      @id = id
      @refusals = 0
      @faults = 0
      @eliminated = false
      @state = :draft
      @dog_time = nil
    end

    def start(round_id, dog_id)
      apply ScribingStarted.new(data: {
        scribesheet_id: @id,
        round_id: round_id,
        dog_id: dog_id
      })
    end

    def record_refusal
      apply RefusalRecorded.new(data: {
        scribesheet_id: @id
      })
    end

    def record_fault
      apply FaultRecorded.new(data: {
        scribesheet_id: @id
      })
    end

    def record_elimination
      apply EliminationRecorded.new(data: {
        scribesheet_id: @id
      })
    end

    def record_dog_time(seconds, hundredths)
      apply DogTimeRecorded.new(data: {
        scribesheet_id: @id,
        seconds: seconds,
        hundredths: hundredths
      })
    end

    def finish
      raise DogTimeNotRecorded unless dog_time_recorded?

      apply ScribingFinished.new(data: {
        scribesheet_id: @id,
        dog_id: @dog_id,
        round_id: @round_id,
        faults: @faults,
        refusals: @refusals,
        seconds: @dog_time.seconds,
        hundredths: @dog_time.hundredths,
        eliminated: @eliminated
      })
    end

    def dog_time_recorded?
      !@dog_time.nil?
    end

    on ScribingStarted do |event|
      @round_id = event.data[:round_id]
      @dog_id = event.data[:dog_id]
      @state = :started
    end

    on RefusalRecorded do |event|
      @refusals += 1
    end

    on FaultRecorded do |event|
      @faults +=1
    end

    on EliminationRecorded do |event|
      @eliminated = true
    end

    on ScribingFinished do |event|
      @state = :finished
    end

    on  DogTimeRecorded do |event|
      @dog_time = DogTime.new(
        event.data[:seconds],
        event.data[:hundredths]
      )
    end
  end
end