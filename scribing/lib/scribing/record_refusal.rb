module Scribing
  class RecordRefusal < Command
    attribute :scribesheet_id, Types::UUID

    alias :aggregate_id :scribesheet_id
  end
end