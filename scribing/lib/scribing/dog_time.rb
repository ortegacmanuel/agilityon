module Scribing
  class DogTime
    attr_reader :seconds, :hundredths

    def initialize(seconds, hundredths)
      @seconds = seconds
      @hundredths = hundredths
    end

    def to_s
      "#{@seconds}.#{@hundredths}"
    end
  end
end