module Scribing
  class OnRefusalRecorded
    include CommandHandler

    def initialize()
    end

    def call(command)
      with_aggregate(Scribesheet, command.aggregate_id) do |scribesheet|
        scribesheet.record_refusal
      end
    end
  end
end