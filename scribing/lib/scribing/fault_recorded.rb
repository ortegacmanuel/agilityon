module Scribing
  class FaultRecorded < Event
    attribute :scribesheet_id, Types::UUID
  end
end