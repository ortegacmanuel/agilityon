module Scribing
  class Configuration
    def call(bus)
      bus.register(Scribing::StartScribing, Scribing::OnScribingStarted.new)
      bus.register(Scribing::RecordRefusal, Scribing::OnRefusalRecorded.new)
      bus.register(Scribing::RecordFault, Scribing::OnFaultRecorded.new)
      bus.register(Scribing::RecordDogTime, Scribing::OnDogTimeRecorded.new)
      bus.register(Scribing::RecordElimination, Scribing::OnEliminationRecorded.new)
      bus.register(Scribing::FinishScribing, Scribing::OnScribingFinished.new)
    end
  end
end
